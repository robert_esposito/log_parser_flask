import os
import time
import sqlite3
from sqlite3 import Error

DB_PATH = os.path.join(os.getcwd(), 'static\\fileDB.db')


def wait_until_midnight():
    t = time.localtime()
    current_time = time.strftime('%H%M', t)
    if int(current_time[:2]) >= 23:
        timer()
    else:
        print(f'Sleeping for 1 hour\nCurrent time: {current_time[:2]}:{current_time[2:]}\n')
        time.sleep(3600)
        wait_until_midnight()


def timer():
    delete_db()
    create_connection()
    add_table()
    print('Sleeping for 24 hours...\n')
    time.sleep(86400)
    timer()


def delete_db():
    print('Removing previous database...\n')
    os.remove(DB_PATH)


def create_connection():
    conn = None
    try:
        conn = sqlite3.connect(DB_PATH)
    except Error as e:
        print(e)
    finally:
        if conn:
            conn.close()
            print(f'Database created.\nVersion: {sqlite3.version}\n')


def add_table():
    conn = sqlite3.connect(DB_PATH)
    cursor = conn.cursor()
    cursor.execute('CREATE TABLE fileData (name TEXT NOT NULL, i INTEGER, line TEXT);')
    print(f'Table fileData created in {DB_PATH}.\n')
    conn.commit()
    conn.close()


if __name__ == '__main__':
    wait_until_midnight()
